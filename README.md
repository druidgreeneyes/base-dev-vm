# Instructions

Make sure you have Vagrant and Virtualbox installed.

Clone this repo.

navigate to cloned directory.

run `vagrant up`

installation will take while.

run `vagrant halt`

change `vb.gui=false` to `vg.gui=true` in Vagrantfile

run `vagrant up`.

Develop.

## Deployment

Best practice will be to have seperate vagrant boxes that imitate production environments and share to folders on the host machine. To deploy from this machine directly to those, symlink them under the `data` directory.

## Tweaking

This machine will seem slow at first, and you'll have to make sure you choose the Cinnamon with software rendering when you log in.

You can make it less slow by adding tweaks to the Vagrantfile:

```ruby
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
    # Customize the amount of memory on the VM:
    # vb.memory = "1024"
    # Customize the number of cpus allocated to the VM:
    # vb.cpus = 2
    # Further customization using VBoxManage:
    # .customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
  end
```

for VBoxManage guidance, see here:

https://www.vagrantup.com/docs/virtualbox/configuration.html

and here:

https://www.virtualbox.org/manual/ch08.html