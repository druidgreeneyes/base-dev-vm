
#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install -y \
	cinnamon-desktop-environment \
	zsh \
	git

useradd -G sudo -d /home/vagrant -s /bin/zsh -m vagrant
echo vagrant:vagrant | chpasswd
